<?php
/*
	Plugin Name: Modesco Slider Extension
	Description: Расширение для слайдера на главной странице.
	Version: 0.1
	Author: Modesco
*/

if ( !defined( 'ABSPATH' ) ) die( '-1' );

class СustomSelectPostsInit {

	private static $_instance,
				   $local_constants;

	private function __construct() {
		add_action('init', array($this, 'set_local_constants'));
		add_action('init', array($this, 'init_backend'));
		add_action('init', array($this, 'init_frontend'));
		add_action('admin_enqueue_scripts', array($this, 'init_admin_js_css'));
	}

	static function init() {
		if(is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	function set_local_constants() {
		$arr = array(
			'ADDS_ROOT_DIR' => __DIR__,
			'ADDS_ROOT_URL' => plugin_dir_url( __FILE__ ),
		);


		self::$local_constants = $arr;
	}
	static function get_constant($name) {
		return self::$local_constants[ $name ];
	}

	function init_backend() {
		require_once self::get_constant('ADDS_ROOT_DIR') .'/inc/backend.php';
	}
	function init_frontend() {
		require_once self::get_constant('ADDS_ROOT_DIR') .'/inc/frontend.php';
	}
	function init_admin_js_css() {
		if (get_current_screen()->id === 'toplevel_page_custom-select-posts-settings') {
			wp_enqueue_script('cspfmp__js', self::get_constant('ADDS_ROOT_URL') .'/js/backend.js',array('jquery'));
			wp_enqueue_style('cspfmp__css', self::get_constant('ADDS_ROOT_URL') .'/css/backend.css');
			wp_localize_script(
				'cspfmp__js',
				'cspfmp__ajaxurl',
				array(
					'url'   => admin_url('admin-ajax.php'),
					'nonce' => wp_create_nonce('cspfmp__nonce')
				)
			);
		}
	}
}

СustomSelectPostsInit::init();
