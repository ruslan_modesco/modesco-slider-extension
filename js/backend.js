(function($){
	'use strict';
	$(document).on('ready',function(){
		//radio
		check_dependency();
		$(this).on('change','.cspfmp__trigger', function() {
			check_dependency();
		})
		.on('submit','#cspfmp__form', function(e) {
			e.preventDefault();
			var posts = '';
			$( ".cspfmp__select_posts option:selected" ).each(function() {
		      posts += $(this).val() + " ";
		    });
			var data_form = JSON.stringify($(this).serializeArray());
			var data = {
				action: 'cspfmp__save_settings',
				data_form: data_form,
				posts: posts,
				dataType: 'html',
				nonce: cspfmp__ajaxurl.nonce
			}
			$.post(cspfmp__ajaxurl.url, data, function(){})
			.success(function(data){
				if (data == 'success') {
					show_success();
					setTimeout(hide_success, 1000);
				}
			});
		});
	});
	function check_dependency() {
		var trigger = $('#cspfmp__form .cspfmp__trigger:checked').val();
		switch (trigger) {
			case 'last':
				$('.cspfmp__dependcy').hide();
		    	$('.cspfmp__dependcy_last').show();
		    break;
			case 'custom':
				$('.cspfmp__dependcy').hide();
				$('.cspfmp__dependcy_custom').show();
		    break;
		  	default:
				$('.cspfmp__dependcy').hide();
		}
	}
	function show_success() {
		$('.cspfmp__message').html('Сохранено');
	}
	function hide_success() {
		$('.cspfmp__message').html('');
	}
})(jQuery);