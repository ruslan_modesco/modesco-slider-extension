<?php
if ( !defined( 'ABSPATH' ) ) die( '-1' );

class СustomSelectPostsBackend {

	private static $_instance,
				   $options;

	private function __construct() {
		$this->get_options();
		add_action('admin_menu', array($this, 'generate_menu'));
		add_action('wp_ajax_cspfmp__save_settings', array($this, 'save_settings'));
	}

	static function init() {
		if(is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	function generate_menu() {
		add_menu_page(
			__('Слайдер главной страницы', ''),
			__('Слайдер главной страницы', ''),
			'administrator',
			'custom-select-posts-settings',
			array($this, 'settings__html')
		);
	}
	function settings__html() { ?>
		<div class="cspfmp__wrapper">
			<h2>Страница настроек слайдера главной страницы</h2>
			<form id="cspfmp__form">
				<?php $this->get_trigger(); ?>
				<?php $this->get_count(); ?>
				<?php $this->get_list_posts(); ?>
				<?php $this->get_submit(); ?>
				<div class="cspfmp__message"></div>
			</form>
		</div>
	<?php }


	private function get_options() {
		$default = array(
			'trigger' => 'off',
			'count'   => 11
		);
		$saved = get_option('custom_select_posts_for_main_page');
		if (is_array($saved)) {
			$options = array_merge($default, $saved);
		} else {
			$options = $default;
		}
		self::$options = $options;
	}

	static function get_option($name) {
		return self::$options[ $name ];
	}

	function get_trigger() {
		$arr = array(
			'off'    => 'Стандарт',
			'last'   => 'Последние записи',
			'custom' => 'Выбрать записи'
		);
		$option = self::get_option('trigger');
		echo '<div class="cspfmp__option">';
		foreach ($arr as $value => $title) {
			echo '<label><input type="radio" name="trigger" value="'. $value .'"'. ($value === $option ? ' checked' : '') .' class="cspfmp__trigger">'. $title .'</label><br />';
		}
		echo '</div>';
	}
	function get_count() {
		$count = self::get_option('count');
		echo '<div class="cspfmp__option cspfmp__dependcy cspfmp__dependcy_last">';
			echo '<label>Количество постов: <input type="text" name="count" value="'. $count .'" placeholder="11" /></label>';
		echo '</div>';
	}
	function get_list_posts() {
		$args = array(
			'numberposts' => -1,
		);
		$posts = get_posts($args);
		$saved = self::get_option('posts');
		$posts_array = explode(' ', $saved);
		echo '<div class="cspfmp__option cspfmp__dependcy cspfmp__dependcy_custom">';
			echo '<select multiple="multiple" size="10" class="cspfmp__select_posts">';
			foreach ($posts as $key => $post) {
				echo '<option value="'. $post->ID .'"'. (is_array($posts_array) && in_array($post->ID, $posts_array) ? ' selected' : '') .'>'. $post->post_title .'</option>';
			}
			echo '</select>';
		echo '</div>';
	}
	function get_submit() {
		echo '<div class="cspfmp__option">';
			echo '<input type="submit" value="Сохранить изменения" />';
		echo '</div>';
	}
	function save_settings() {
		if (!wp_verify_nonce(htmlentities($_POST['nonce']), 'cspfmp__nonce')) {
			echo 'error';
			wp_die();
		}
		$a = str_replace( '\\', '', $_POST['data_form'] );
		$data = array_reduce(json_decode($a, false), function($acc, $obj){
			$acc[ $obj->name ] = $obj->value;
			return $acc;
		},[]);
		$data['posts'] = esc_attr(trim($_POST['posts']));

		update_option('custom_select_posts_for_main_page', $data);
		echo 'success';
		wp_die();
	}

	function test() {
	}
}

if (is_admin()) {
	СustomSelectPostsBackend::init();
}