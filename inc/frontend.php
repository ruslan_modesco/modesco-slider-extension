<?php
if ( !defined( 'ABSPATH' ) ) die( '-1' );

class СustomSelectPostsFrontend {

	private static $_instance,
				   $options;

	private function __construct() {
		$this->get_options();
	}

	static function init() {
		if(is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	private function get_options() {
		$default = array(
			'trigger' => 'off',
			'count'   => 11
		);
		$saved = get_option('custom_select_posts_for_main_page');
		if (is_array($saved)) {
			$options = array_merge($default, $saved);
		} else {
			$options = $default;
		}
		self::$options = $options;
	}

	static function get_option($name) {
		return self::$options[ $name ];
	}

	static function get_slider_html() {
		$trigger = self::get_option('trigger');
		switch ($trigger) {
			case 'last':
				$html = self::get_last();
				break;
			case 'custom':
				$html = self::get_custom();
				break;
			
			default:
				$html = '';
				break;
		}

		echo $html;
	}

	static function check_status() {
		$trigger = self::get_option('trigger');

		return ($trigger === 'off' ? false : true);
	}

	static function get_last() {
		$count = self::get_option('count');
		$count = ($count ? $count : 11);
		$args  = array(
			'numberposts' => $count,
		);
		$posts = get_posts($args);
		$html  = '';

		foreach ($posts as $key => $post) {
			$html .= '<div class="slide" style="background-image: url('. get_the_post_thumbnail_url($post->ID, "middle") .')"><a href="'. get_the_permalink($post->ID) .'">'. get_the_title($post->ID) .'</a></div>';
		}

		return $html;
	}
	static function get_custom() {
		$posts = self::get_option('posts');
		$html  = '';

		if ($posts) {
			$posts_arr = explode(' ', $posts);

			if (is_array($posts_arr)) {
				foreach ($posts_arr as $id) {
					$html .= '<div class="slide" style="background-image: url('. get_the_post_thumbnail_url($id, "middle") .')"><a href="'. get_the_permalink($id) .'">'. get_the_title($id) .'</a></div>';
				}
			}
		}

		return $html;
	}
}

СustomSelectPostsFrontend::init();